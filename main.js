alert('Version 0.0.2')

var db = new Dexie("save");
db.version(1).stores({
	save: 'key,data'
});

const possibilities = [
	0, 0, 0, 0, 0,
	1, 1, 1, 1, 1,
	2, 2, 2, 2, 2,
	3, 3, 3, 3, 3,
	4, 4, 4, 4, 4, 4, 4, 4,
	5, 5, 5, 5, 5, 5, 5,
	7
]

window.onerror = function(message, source, lineno, colno, error) {
	alert('Error')
	alert(message)
	alert(source)
	alert(lineno)
	alert(colno)
	alert(error)
}

let SPEED = location.hash ? parseInt(location.hash.substr(1)) * 2 : 16

// setTimeout(function() {
// 	if(localStorage.getItem('first') === 'yes') {
// 		openState('save', mainCanvas)
// 	} else {
// 		localStorage.setItem('first', 'yes')
// 	}
// }, 500)

function LOAD() {
	// firebase.database().ref('/').once('value').then(function(snapshot) {
	// console.log(snapshot.val().save.substr(0, 10))
	// gameboy = new GameBoyCore(mainCanvas, "");
	// gameboy.returnFromState(JSON.parse(snapshot.val().save));
	// run();
	// });


	db.save.get(0).then(function(data) {
		if (typeof data != "undefined") {
			console.log("DB")
			gameboy = new GameBoyCore(mainCanvas, "");
			gameboy.returnFromState(JSON.parse(data.data));
			run();
			alert('Loaded')
		} else {
			alert('No data found')
		}

	})
}


function SAVE() {
	// // firebase.database().ref('/save').set("none").then(function() {
	//     pause()
	//     firebase.database().ref('/save').set(JSON.stringify(gameboy.saveState())).then(function() {
	// 		alert('saved')
	// 	})
	// })



	db.save.put({
		key: 0,
		data: JSON.stringify(gameboy.saveState())
	}).then(function() {
		alert('Saved')
	})
}

setTimeout(LOAD, 1000)

// setInterval(SAVE, 30000)

window.addEventListener('click', function(e) {
	SAVE()
})


setInterval(() => {
	let key = possibilities[
		Math.floor(Math.random() * possibilities.length)
	]

	GameBoyJoyPadEvent(key, true);
	setTimeout(function() {
		GameBoyJoyPadEvent(key, false)
	}, 150 / SPEED)
	// let lines = sidebar.innerHTML.split('<br>')
	// lines.splice(1,1)
	// lines.push(key[0].toUpperCase() + key.substring(1))
	// sidebar.innerHTML = lines.join('<br>')
}, 200 / SPEED)



if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('service-worker.js')
		.then(function(registration) {
			console.log('Registered:', registration);
		})
		.catch(function(error) {
			console.log('Registration failed: ', error);
		});
} else {
	console.log('no serviceworker')
}
