var CACHE_NAME = 'static-cache';
var urlsToCache = [
	'dexie.js',
	'index.html',
	'.DS_Store',
	'images',
	'images/icons-512.png',
	'images/icons-180.png',
	'js',
	'js/GameBoyIO.js',
	'js/GameBoyCore.js',
	'other',
	'other/XAudioServerMediaStreamWorker.js',
	'other/swfobject.js',
	'other/terminal.js',
	'other/json2.js',
	'other/base64.js',
	'other/windowStack.js',
	'other/resampler.js',
	'other/gui.js',
	'other/resize.js',
	'other/XAudioServer.js',
	'main.js',
	'manifest.json',
	'red.gb',
	'service-worker.js',
	'rom64'
].map(a=>'/'+a);
self.addEventListener('install', function(event) {
	event.waitUntil(
		caches.open(CACHE_NAME)
		.then(function(cache) {
			return cache.addAll(urlsToCache);
		})
	);
});


self.addEventListener('fetch', function(event) {
	event.respondWith(
		caches.match(event.request)
		.then(function(response) {
			return response || fetchAndCache(event.request);
		})
	);
});

function fetchAndCache(url) {
	return fetch(url)
		.then(function(response) {
			// Check if we received a valid response
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return caches.open(CACHE_NAME)
				.then(function(cache) {
					cache.put(url, response.clone());
					return response;
				});
		})
		.catch(function(error) {
			console.log('Request failed:', error);
			// You could return a custom offline 404 page here
		});
}
